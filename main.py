#!/usr/bin/env python2

from controller import Controller
from temp import Temp
from time import sleep
from pwm import Pwm
from threading import Thread
import sys

def runner(queue):
    while True:
        queue.get(True)
        temp.tic()
        print "{}, {}, {}, {}".format(temp.get("pod0"), ctl._setpoint, ctl._dc, ctl._int)
        sys.stdout.flush()
        ctl.tic()

#setting up temperature measurement
temp = Temp()
temp.addPod("pod0", ["28-0115811e49ff","28-80000001147d"])

#setting up the pwm-output
pwm = Pwm()
pwm.addPod("pod0", 11)

#setting up the controller
ctl = Controller("pod0", temp.get, pwm.setpwm)
ctl._setpoint = 56.0

thread = Thread(target= runner, args=(pwm.ctltrigger,))
thread.start()
thread.join()
