#!/usr/bin/env python2

import controller 
import matplotlib.pyplot as plt

_temp = 20.0
_dc = 0.0

dt = 10.0 # [s]

c_p = 4.183 # [J/g*K]
v = 1.0 # [ml / g]
P_H = 300.0 # [W]
P_P = 10.0 # [W]
th_u = 20.0 # [C]
V = 10e3 # [ml]
g_th = 1.0 # [W/K]

def temp(name):
  global _temp
  return _temp

def pwm(name, dc):
  global _temp
  global _dc 
  _dc = dc
  _temp = _temp \
          + (1.0 / c_p) * (P_H * dt * dc + P_P) * ( 1.0 / (V * v)) \
          - (_temp - th_u) * g_th * dt * (1.0 / (c_p * V * v))

ctl = controller.Controller("namen", temp, pwm)

ctl._setpoint = 56.0

dc_l = [0.0]
temp_l = [_temp]
int_l = [0]

len_ = 1000
for i in range(len_):
  ctl.tic()
  print "{:=4.0f}s: {:=3.2f}degC, {}, {}".format(i*dt, _temp, _dc, ctl._int)
  dc_l.append(_dc)
  temp_l.append(ctl._setpoint - _temp)
  int_l.append(ctl._int)


print "min. temp delta: {}".format(min(temp_l))
offs = 00

#plt.xkcd()

fig, ax1 = plt.subplots()
ax1.plot(range(len(temp_l[offs:])), temp_l[offs:], 'r')
ax1.set_ylabel('temp diff')
ax1.grid(True)

ax2 = ax1.twinx()
ax2.plot(range(len(dc_l[offs:])), dc_l[offs:], 'b', range(len(dc_l[offs:])), int_l[offs:], 'g')
ax2.set_ylabel('dc')
ax2.set_ylim([0,1])
plt.show()

#plt.plot(range(len(temp_l)), temp_l, 'r', range(len(dc_l)), dc_l, 'b')
#plt.show()


