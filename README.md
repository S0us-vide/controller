Controller for a sous-vide coocker


Currently the used temperature-sensors GPIO-pins and so on must be changed in the main.py.


The following external electrical components must be present:
* 1-Wire Temperature Sensor on e.g. as shown here: https://www.raspiprojekt.de/machen/basics/schaltungen/9-1wire-mit-temperatursensor-ds18b20.html
  * The Sensor ID must be set in the main.py.
  * 1-Wire on the Raspberry needs a PullUp of about 1kOhm to 3.3V.
* Solid-State relais on Pin 11 of the header P1.
  * The tested solid-state relais needed a driving voltage of 5V instead of 3.3V from the PI so an inverting level-converter using a NPN-transistor and a 200R Pull-Up to 5V was used.

* All the needed Pins for 5V, 3.3V and GND can be found e.g. here: http://blog.braier.net/wp-content/uploads/2014/08/raspberry-pi-rev2-gpio-pinout.jpg