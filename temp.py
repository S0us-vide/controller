#!/usr/bin/env python2

def getTemp(w1id):
    try:
        with open("/sys/bus/w1/devices/{}/w1_slave".format(w1id)) as sensor:
            data = sensor.readlines()
            if len(data) == 2:
                if "YES" in data[0]:
                    return float(((data[1].split(" "))[-1]).split("=")[-1]) / 1000.0
        return False
    except IOError:
        return False
    return False

class Temp:
    pods = {}

    current = {}

    def addPod(self, name, sensors):
        """Adds a pod to the temperature-sensor abstraction.
        name: Name of the pod
        sensors: list of 1-wire IDs to use for this pod"""
        self.pods[name] = sensors

    def tic(self):
        """Gets fresh new temperatures for all registred sensors"""
        self.current = {}
        for pod, sensors in self.pods.iteritems():
            temp = [getTemp(x) for x in sensors]
            broken = temp.count(False)
            if broken > 0:
                print "{}: {} of {} sensors could not be read. Better check them now!".format(pod, broken, len(temp)i)
            if broken == len(temp):
                self.current[pod] = False
            else:
                mi = temp[0]
                ma = temp[0]
                for i in range(1, len(temp)):
                    if temp[i] < mi and temp[i] is not False:
                        mi = temp[i]
                    if temp[i] > ma and temp[i] is not False:
                        ma = temp[i]
                if (ma - mi) > 3.0:
                    print "{}: Sensor differ over 3.0K. Falling back and using hottest!".format(pod)
                    self.current[pod] = ma
                else:
                    self.current[pod] = sum(temp) / float(len(temp)-broken)

    def get(self, name):
        """Returns a temperature for a sensor"""
        return self.current[name] 
