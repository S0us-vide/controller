#!/usr/bin/env python2

from threading import Thread
from time import sleep
import RPi.GPIO as GPIO
import Queue

class Pwm:
    pods = {}
    current = {}

    period = None
    trigdc = None
    tic = None
    tics = None

    ctltrigger = None

    def addPod(self, name, gpio):
        """Adds a pod to the PWM-generator. gpio will be set to outout and used as soft pwm"""
        # set gpio to output and low
        self.pods[name] = {"pin":gpio}
        GPIO.setup(gpio, GPIO.OUT)
        GPIO.output(gpio, GPIO.HIGH)

        self.current[name] = 0.0


    def setpwm(self, name, dc):
        """Sets a duty cycle for a pod for the next cycle"""
        dc = min(1.0, max(0.0, dc))
        self.current[name] = dc

    def _runner(self):
        while True:
            triggered = False
           
           # Begin new period with all channels high
            for pod, params  in self.pods.iteritems():
                GPIO.output(params["pin"], GPIO.LOW)

            
            for i in range(self.tics):
                dc = float(i) / self.tics
                
                # Processing trigger for new measurements
                if dc > self.trigdc and not triggered:
                    self.ctltrigger.put_nowait(True)
                    triggered = True

                # outputting soft PWM
                for pod, params in self.pods.iteritems():
                    if dc >= self.current[pod]:
                        GPIO.output(params["pin"], GPIO.HIGH)
                        

                sleep(self.tic)

        
    def __init__(self, trigdc=0.5, period=10.0, tics=100):
        self.trigdc = trigdc
        self.period = period
        self.tic = period/tics
        self.tics = tics

        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)

        self.ctltrigger = Queue.Queue()

        thread = Thread(target= self._runner, args=())
        thread.start()

