#!/usr/bin/env python2

class Controller:
    _name = ""
    
    _i = [0.04,0.120]
    _i_limits = (-0.2, 0.35)
    _int = 0.0
  
    _p = 0.5

    _limits = (0.0, 1.0)

    _setpoint = 0.0

    _temp = None
    _pwm  = None

    _dc = 0.0

    def tic(self):
        temp = self._temp(self._name)
        diff = self._setpoint - temp

        if temp is False:
            self._dc = 0
            print "{}: Temperature error.Setting DC=0%".format(self._name)
        else:
            if diff > 0:
                #current temperature is below setpoint
                self._int = self._int + diff * self._i[0]
            else:
                self._int = self._int + diff * self._i[1]

            self._int = min(self._i_limits[1], max(self._i_limits[0], self._int))

            dc = diff * self._p + self._int

            dc = min(self._limits[1], max(self._limits[0], dc))

            self._dc = dc

        self._pwm(self._name, dc)

    def __init__(self, name, temp, pwm):
        self._name = name
        self._temp = temp
        self._pwm = pwm


